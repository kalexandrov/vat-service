#!/bin/bash

echo Running service...

docker run --rm -it -v "$(pwd)":/app -e "GOPATH=/gopath" -w /app -p 8000:8000 google/golang sh -c 'go run Main.go'