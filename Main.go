package main

import (
    "encoding/json"
    "log"
    "net/http"
    "time"
)

type Country struct {
	Iso2 string `json:"iso2"`
	Iso3 string `json:"iso3"`
}

type VatRate struct {
    Rate float32 			`json:"rate"`
    Starting *time.Time 	`json:"starting"`
    Ending *time.Time 		`json:"ending,omitempty"`
}

type CountryVat struct {
	Country Country `json:"country"` 
    Rates []VatRate `json:"rates"`
}

var vatRates = []CountryVat{
	CountryVat {
		Country: Country { 
			Iso3: "NLD", 
			Iso2: "NL",
		},
		Rates: []VatRate {
			VatRate { 
				Rate: 0.21, 
				Starting: dateOf(2000, 01, 01),
			},
		},
	},
	CountryVat {
		Country: Country { 
			Iso3: "DEU", 
			Iso2: "DE" ,
		},
		Rates: []VatRate {
			VatRate { 
				Rate: 0.19, 
				Starting: dateOf(2000, 01, 01), 
				Ending: dateOf(2028, 01, 01),
			},
		},
	},
}

func dateOf(year int, month time.Month, day int) *time.Time {
	date := time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
	return &date
}

func buildVat() {

}

func vatHandler(res http.ResponseWriter, req *http.Request) {
    data, _ := json.MarshalIndent(vatRates, "", "  ")
    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func main() {
    log.Println("Listening on this host: http://localhost:8000")

    http.HandleFunc("/vat", vatHandler)
    err := http.ListenAndServe(":8000", nil)
    if err != nil {
        log.Fatal("Unable to listen on :8000: ", err)
    }
}