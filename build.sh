#!/bin/bash

echo Building...

mkdir -p out
docker run --rm -it -v "$(pwd)":/app -w /app google/golang sh -c 'CGO_ENABLED=0 go build -a --installsuffix cgo --ldflags="-s" -o out/main'

